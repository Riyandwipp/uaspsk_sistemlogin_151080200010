<?php 
//koneksi ke daabase
$conn = mysqli_connect("localhost", "root", "", "phpku");



function query($query) {
	global $conn;
	$result = mysqli_query($conn, $query);
	$rows = [];
	while( $row = mysqli_fetch_assoc($result) ) {
		$rows[] = $row;
	}
	
	return $rows;
}



function tambah($data) {
	global $conn;
	$nama = htmlspecialchars($data["nama"]);
	$nrp = htmlspecialchars($data["nrp"]);
	$email = htmlspecialchars($data["email"]);
	$jurusan = htmlspecialchars($data["jurusan"]);
	$gambar = upload();
	if( !$gambar) {
		
		return false;
	}

	//query insert data
	$query = "INSERT INTO mahasiswa
				VALUES 
				('', '$nama', '$nrp', '$email', '$jurusan', '$gambar')
				";
	mysqli_query($conn, $query );

	return mysqli_affected_rows($conn);
 }

function upload() {

	$namaFile = $_FILES ['gambar']['name'];
	$ukuran = $_FILES ['gambar']['size'];
	$error = $_FILES ['gambar']['error'];
	$tmpName = $_FILES ['gambar']['tmp_name'];

	// cek apaka tidak ada gambar yg diupload
	if ( $error === 4 ) {
		echo " <script>
				alert('pilih gambar terlebih dahulu !');
				</script> ";

				return false;
	}

	//cek apakah yang di upload adl gambar

	$ekstensifilevalid = ['jpg', 'jpeg', 'png'];
	$ekstensifile = explode('.', $namaFile);
	$ekstensifile = strtolower(end($ekstensifile) );


		if ( !in_array($ekstensifile, $ekstensifilevalid))
		{

			echo " <script>
				alert('yang anda upload bukan gambar !');
				</script> ";
				return false;
		
		}
		//cek ukuran gambar
		if ( $ukuran > 1000000000 ) {
			
			echo "<script>
			alert('ukuran gambar terlalu besar');
			</script>";
			
			return false;
		}

		//lolos pengecekan upload file
		// generate nama baru
		$namafilebaru = uniqid();
		$namafilebaru .= '.';
		$namafilebaru .= $ekstensifile;


		move_uploaded_file($tmpName, 'gambar/' . $namafilebaru);

		return $namafilebaru;


}



function hapus($id) {
	global $conn;
	mysqli_query($conn, "DELETE FROM mahasiswa WHERE id = $id");

	return mysqli_affected_rows($conn);
}



function ubah($data) {
 global $conn;

 	$id = $data["id"];
 	$nama = htmlspecialchars($data["nama"]);
	$nrp = htmlspecialchars($data["nrp"]);
	$email = htmlspecialchars($data["email"]);
	$jurusan = htmlspecialchars($data["jurusan"]);
	$gambarlama = htmlspecialchars($data["gambarlama"]);
	
	//cek apakah user pilih gambar baru atau tidak

	if( $_FILES['gambar']['error'] === 4 ) {
		$gambar = $gambarlama;
	} else{
		$gambar = upload();
	}

	

	$query = "UPDATE mahasiswa SET
				nama = '$nama',
				nrp = '$nrp',
				email = '$email',
				jurusan = '$jurusan',
				gambar = '$gambar'
				WHERE id = $id
				";
	mysqli_query($conn, $query );

	return mysqli_affected_rows($conn);

   
}


function cari($keyword) {
	$query = "SELECT * FROM mahasiswa
				WHERE
				nama LIKE '%$keyword%' OR
				nrp LIKE '%$keyword%' OR
				nama LIKE '%$keyword%' OR
				email LIKE '%$keyword%' OR
				jurusan LIKE '%$keyword%'
				";
	return query($query);
}



function registrasi($data) {
	global $conn;
	$nama_mhs = strtolower(stripslashes($data["nama_mhs"]));
	$nim_mhs = strtolower(stripslashes($data["nim_mhs"]));
	$username = strtolower(stripslashes($data["username"]));
	$password = mysqli_real_escape_string($conn, $data["password"]);
	$password2 = mysqli_real_escape_string($conn, $data["password2"]);
	$level = strtolower(stripslashes($data["level"]));
//cek username sudah ada atau belum
	$result = mysqli_query($conn, "SELECT username FROM mhs WHERE username = '$username'");
	//$login = mysqli_query("SELECT * FROM mhs where username = $username and password = $password");

	if( mysqli_fetch_assoc($result) ) {
		echo "<script>

		alert('username sudah terdaftar!');

		</script>";

		return false;

	}

//cek konfirmasi password
	if( $password !== $password2 ){

		echo "<script>
			alert('konfirmasi password tidak sesuai');

		</script>";

		return false;
	}
//enkripsi password
	
	$password = password_hash($password, PASSWORD_DEFAULT);

//tambahkan user baru ke database

	mysqli_query($conn, "INSERT INTO mhs VALUES('','$nama_mhs','$nim_mhs', '$username', '$password','$level')");

	return mysqli_affected_rows($conn);
}

 ?>