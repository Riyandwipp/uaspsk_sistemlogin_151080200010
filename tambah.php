<?php 
session_start();

if( !isset($_SESSION["login"]) ) {
	header("Location: login.php");
	exit;
}

require 'functions.php';

//cek apakah tombol submit sudah ditekan atau belum
if (isset($_POST["submit"]) ){
//cek data berhasil ditambahkan atau tidak

	if (tambah($_POST) > 0) {
		echo "
		<script>
			alert('Data berhasil ditambahkan !');
			document.location.href = 'indeks.php';
		</script>

		";
	} else {
		echo "
		<script>
			alert('Data gagal ditambahkan !');
			document.location.href = 'indeks.php';
		</script>

		";
		echo "<br>";
		echo mysqli_error($conn);
	}
		
}

 ?>

<!DOCTYPE html>
<html>
<head>
	<title>Tambah Data</title>
	<style>
		label {
			display: block;
		}
	</style>
</head>
<body bgcolor="skyblue">

	<h1>Tambah Data</h1>

	<form action="" method="post" enctype="multipart/form-data">
		<ul>
			<li>
				<label for="nama">Nama Mahasiswa</label>
				<input type="text" name="nama" id="nama" required>
			</li>

			<li>
				<label for="nrp">NRP</label>
				<input type="text" name="nrp" id="nrp"
				required>
			</li>
			<li>
				<label for="email">EMAIL</label>
				<input type="text" name="email" id="email" required>
			</li>

			<li>
				<label for="jurusan">Jurusan</label>
				<input type="text" name="jurusan" id="jurusan" required>
			</li>

			<li>
				<label for="gambar">Gambar</label>
				<input type="file" name="gambar" id="gambar" required>
			</li>
			<li>
				<button type="submit" name="submit">Tambah Data!</button>
			</li>
		</ul>

	</form>

</body>
</html>